**Log Report IDP Aerospace Design 2021**

Log 1 (Week 3)

 

*Agenda*

 

Friday - 29/10/2021

I was assigned to Software and Control System Team and Group 6 (mixed group) along with others. The main reason of the meeting is update the progress made by the Flight System Integration Team and also Design Structure Material Team. The meeting began with the progress presentation by fellow project mate Adlina on the material list for building the airship. Justifications was given for the selected items for the reference of others. The availability of the components are discussed and checked. Some of the components were from overseas so the delivery time would be a challenge to complete the project. Hence, ways to overcome the challenge has been discussed. Then, our instructor assigned every team to create gann chart to keep track of the progress of the group. He explained the significant of gann chart. So the gann chart completed by 5 pm.

 

Monday - 01/10/2021

A small discussion was held in the IDP Software Whatsapp team among the team members about the task had to be accomplished. It was discussed that the control system could not be designed unless the components of the airship is finalized. Still all the team members should have the overall idea of the structure of the airship control system structure. Also, a meeting with Mr Azizi was planned in order to get in dept information about the project.

 

 

*Team Decisions*

 

Since the components for the airship, such as the flight controller and the servo for the sprayer, yet finalized, the control system could not be designed. Besides, meeting with Mr Azizi have not arranged. Hence, the progress decided to perform is to do some background study about the control system of the airship. I myself have designed the control system of drone during my internship in Malaysia Airlines Berhad. There are some similarities on how a drone and airship work, however the major difference is a drone’s lift is fully depend on its propellers, however an airship’s lift is mainly depends on the helium gas in it. So, in order to understand more about the working principle of the airship, background study has be done.

 

*Next Step*

 

Background study about the airship has to be done to get further idea on designing the control system of the airship. Other than that, the team has decided to wait until the meeting with Mr Azizi and the final confirmation of the components, and observe the further development of other teams.

 

*Comments*

 

There was not much justification given for the components chosen by the Flight Integration team and Structure Materials Teams. Although the needed information for the project is taken from the previous Airship project carried out by Putraspace team, in my opinion proper calculations and justifications for the selection of the components must be done or shown to everyone if they are done. On the other hand, not all the progress has been shown to everyone. The individuals participating the progress of the project in laboratory in Universiti Putra Malaysia, were not updating the progress clearly. As per for me, I could not present to the laboratory session because I am not allowed to be in UPM during phase 3. Several progress has been made in laboratory that is crucial for everyone to know. The updates that we receive is the results not the process. So it is not clear. Still I try my best to present for the laboratory session to keep myself updated.






**Log Report IDP Aerospace Design 2021**

*Log 2 (Week 4)*

 

*Agenda*

 

Friday - 05/11/2021

The first agenda of the meeting was the progress presentation by each team. Each members presented their progress and the current waypoint in the gann chart. Yul from the Software and Control System Team presented behalf of us. He noted that the team has received the reference from Mr Azizi and currently in progress in designing the control system. Then, our instructor, Dr Salah instructed us to gather us in our respective group (colourful group) and discussed among the group members. To make the process easier, Dr Salah assigned a animal to each group. Group 6 was assigned to cat. Then discussion among team mates was carried out. During the discussion, roles for each teammates was assigned. I was assigned as coordinator for the next one week. The tasks for each roles was clarified. Then, progress from each team was updated once again in the group was each members, which are also the representative from the team.

 

Tuesday - 09/11/2021

A small discussion was held in the IDP Software Whatsapp team among the team members about the meeting which has to be held with Mr Azizi. The captain of the team asked about the team member’s availability to conduct the meeting. Each of us announced our availability.  

 

*Team Decisions*

 

During the meeting in the small group, several matters have been discussed. Firstly, we updated our progress in our respective teams. Then, the way of writing engineering log has been discussed. We shared our knowledge on writing the report log among ourselves. Since, the report has to be submitted in Gitlab, we discussed the way of submitting the report log. We decided to arrange a meeting among the group members once we manage to find the method of uploading the report logs. When Dr Salah visited each group during the meeting, he explained the significant of having a discussion in small group. He also pointed the tasks of each person in the group according to their roles.

 

*Next Step*

 

The team has decided to wait until the meeting with Mr Azizi and the final confirmation of the components, and observe the further development of other teams. Besides, the reference given by Mr Azizi has be studied and analyzed to aid us to design our control system.

 

*Comments*

 

The individuals participating the progress of the project in laboratory in Universiti Putra Malaysia, were not updating the progress clearly. So, I have decided to visit the laboratory in person to know the full update of the project and contribute some of my knowledge during my internship in Malaysia Airlines Berhad to this project.







**Log Report IDP Aerospace Design 2021**

Log 3 (Week 5)

 

*Agenda*

 

Friday - 11/11/2021

Yavinaash Naidu, from Flight Intergrity Team, and I persoanlly visited the Engineering laboratory H2.1 in Universiti Putra Malaysia in order to observe the progress persoanlly. The knowledge that we obtained during our internship programme, especially the parameter data sheet and the self built Lithium Ion battery was shared with our fellow classmate, Zaim and our supervisor, Fikri. Then, during the weekly meeting, representative from each team explained the progress made throughout the past one week. After that, we regrouped into our small group and discussed about the progress made each team and also tracked our individual progress in our engineering log book.

 

Thursday - 18/11/2021

A face to face meeting was arranged with Azizi in order to discuss about the control system of the airship that has been developed. Azizi advised us to download the mission planner and familiarize it. He also advised us to learn to calibrate using the mission planner.



*Team Decisions*

 

During the meeting in the small group, several matters have been discussed. Firstly, we updated our progress in our respective teams. Then, we discussed about the method for uploading the engineering log. Since none of the teammates are not aware of the method of uploading the engineering log in Gitlab, we decided to ask around and also try the method that Azizi taught us. After the meeting with Azizi, we decided to pursue our background study on the calibration and study the mission planner. The sources for the mission planner was provided by Azizi.

 

Next Step

 

The team decided to progress to the next step which is the calibration process after the meeting with Azizi. Since the sources for the calibration has been provided, the progress is being eased. Also with the progress report is planned to add in the Gitlab.

 

*Comments*

 

The teamwork among the teammates have been building. As I visited the laboratory on Friday (11/11/2021), I finally know the progress of the project and also able to make my contribution to other team. Since I have not much knowledge on coding, I should make plenty of practise on my programming skills.







**Log Report IDP Aerospace Design 2021**

*Log 4 (Week 6)*

 

*Agenda*

 

Friday - 19/11/2021

As for the meeting session, representatives from every teams presented their progress to other teams and our instructor. Kishore Varma had represented Control System and Software team and presented our progress. Dr Salah pointed that the Control System and Software team is behind schedule where by week 6 we were suppose to present the script for our code. He also advised us to speed up our progress and try to keep up with the planner that we prepared in week 3. Once the presentation from every team has completed, Dr Salah invited the Structure Team for an interview session. The others went to their respective small group chat to discuss about the weekly progress on the engineering log.

 

Tuesday - 23/11/2021

As per the Control System and Software team members decided previously, we visited the laboratory H2.1 UPM for two consecutive days. I could not visit the laboratory on 22/11/2021 (Monday) because I had a Air Breathing Engine Test 1. So I visited the laboratory on 23/11/2021 (Tuesday). Along with Alif, Kishore Varma and Clenneth, I worked on the ways to design the control system in order to control the servo arm of the Ariship.

 

*Team Decisions*

 

The team decided to take in Dr Salah’s advice on speeding up the progress on designing the control system. As per Azizi requested, each of the teammates downloaded the Mission Planner and got familiarized with the software. We calibrated the flight controller using the software and obtained the result. We moved on to the next step which is editing the firmware of the flight controller to control the servo motor.   

 

*Next Step*

 

The firmware of the flight controller has to be studied and analyzed in order to edit it. Since the firmware was designed for a quadcopter, most of its part will be same for the Airship, however the servo has to be programmed in the firmware. So the team has decided to find the way to control the servos.

 

*Comments*

 

One of  the reason the progress of the control system and software is delayed is because the schedule of the Azizi was tight and we were unable to meet him in order to progress in our design of the control system. We tried our best to progress by our own but still we need plenty of sources of programs from Azizi. Still we tried our best to keep up with the progress.







**Log Report IDP Aerospace Design 2021**

*Log 5 (Week 7)*

 

*Agenda*

 

Friday - 26/11/2021

As for the meeting session, representatives from every teams presented their progress to other teams and our instructor. Najmi had represented Control System and Software team and presented our progress. He noted that the members of Control System and Software team has tried the callibration of the flight controller and obtained the result. He also added that the members we are currently working on the firmware of the flight controller. Since we are not informed on the parameters of the Airship like payload weight, thrust needed and others, we are unable to complete the code. Once the presentation from every team has completed, Dr Salah invited the Structure Team for an interview session. The others went to their respective small group chat to discuss about the weekly progress on the engineering log.  

 

*Team Decisions*

 

In order to proceed the next level of work, we need to meet Azizi, but due to his busy schedule we are unable to meet him However, we worked on the remaining work like the framework of the work so that we could run the simulation once the items arrive. Meanwhile, we worked on updating the Software repository in IDP Gitlab page on our updates.  

 

*Next Step*

 

The final work that has to be done is the simulation. The framework of the firmware code has been done.  







Log Report IDP Aerospace Design 2021

Log 6 (Week 8)

 

Agenda

 

Thursday (09/12/2021)

After one week mid semester break, all the team members are required to contribute their part in order to catch up with the task schedule. As per Dr Salah’s advice, one of the team member Sharul reminded that each of the team mates must show full contribution so that the Software and Control System team can produce the expected progress to our instructor. The Software and Control team was reminded to add the information regarding our progress in our Gitlab page so that people from other team could view and know what the Software and Control System team have accomplished so far. Three of the teammates, Kishore, Alif and me, are assigned to do the flow chart of the whole control system of the airship,

 

Friday (10/12/2021)

The finished flowchart of the control system of the ariship together with the sprayer system are attached to the Software and Control System Gitlab page. As for the meeting session, representatives from every teams presented their progress to other teams and our instructor. Sharul represented Control System and Software team and presented our progress. He presented the updates that has been added to the Gitlab page and explain everyone about the progress that has been done regarding the firmware compiling. He also explained that the members of Software and Control System team are waiting for Azizi’s appointment to obtain assist on the compilation of the firmware since we were facing some error in compiling the software.

 

Team Decisions

 

While working on the firmware compilation, the team members of Software and Control System team decided to work on the trial components, like the Ardupilot flight controller, ESCs, and motors. Our goal is successfully upload the firmware in the Ardupilot flight controller, calibrate the radio transmitter and ESCs.

 

Next Step

 

All of  Software and Control System team members are called to present in the laboratory H2.1 in order to carry out the task, which is to upload the firmware in the flight controller and calibrate the radio transmitter and ESCs. All teammates are advised to install Mission Planner configurator software in their respective laptop so that anyone in the team can carry out the calibration although on of the teammates absent.

 

Comment

 

Dr Salah pointed that the Software and Control team is still behind the task schedule and advised that each of the teammate must give their full cooperation and commitment towards the tasks. Hence, one of the teammate, Sharul, advised each of the teammate to present in the laboratory so that everyone could give their full commitment.

 

 

 

**Log Report IDP Aerospace Design 2021**

Log 7 (Week 9)

 

Agenda

 

Monday  (13/12/2021)

The teammates of Software and Control System team gathered in laboratory H2.1 in order to carry out the upload of Ardupilot firmware and calibration of radio transmitter. I was in charge for the upload of firmware and calibration of radio transmitter since I had experience in build quadcopter and working in UAV control system. Other teammates, Alif and Kishore assisted. First of all, the ESCs were soldered to the power distribution board. Then, the ESCs were connected to the Ardupilot. The next step was to connect the FS-i6AB receiver to the flight controller. PPM signal connection was followed to connect to the flight controller. Once the receiver was connected, the flight controller was connected to the laptop to do configuration using Mission Planner software. The Flysky transmitter was used to do the radio calibration. However, the transmitter could not be connected to the receiver.

 

Tuesday (14/12/2021)

The goal that has to achieved by the Software and Control System team is to successfully bind the receiver and the transmitter. After analyzing the connection between the receiver and the flight controller, the teammates successfully bind the receiver and transmitter. Once the transmitter has been calibrated, all the teammates moved on to the next goal which was to calibrate the ESCs. However, it was unable to calibrate the ESCs as the Mission Planner showed the absence of AC3.3. The teammates decided to continue the work on the next day. The problem that we faced on that day was discussed in the Software and Control System team, and we managed to identify the problem which was the flight controller was outdated for the current firmware to be applied.

 

Wedanesday  (15/12/2021)

The goal that has to achieved by the Software and Control System team is to successfully bind the receiver and the transmitter. After analyzing the connection between the receiver and the flight controller, the teammates successfully bind the receiver and transmitter. Once the transmitter has been calibrated, all the teammates moved on to the next goal which was to calibrate the ESCs. However, it was unable to calibrate the ESCs as the Mission Planner showed the absence of AC3.3. The teammates decided to continue the work on the next day.  

 

Thursday  (16/12/2021)

Since the Ardupilot was outdated for the calibration of ESCs, the Software and Control System team requested for the Pixhawk flight controller, which is used for the Ariship project. However, the Software and Control System team were unable to work on the Pixhawk flight controller as it was used for the gondola design. Still the Software and Control System team obtained the permission to work on the Pixhawk Cube flight controller, which was given by Fiqri. With that flight controller, the Software and Control System team successfully callibrated the ESCs. However, in the Pixhawk Cube flight controller, the team were unable to connect the receiver, as a PPM module was needed to convert the PPM signal from the receiver to PWM signal.

 

Friday  (17/12/2021)

 As for the meeting session, representatives from every teams presented their progress to other teams and our instructor. Nava represented Control System and Software team and presented our progress. The problem faced during the calibration of radio transmitter and ESCs were explained to the class and the next step which has to be take was presented in the team.

 

Team Decisions

Once the calibration of the radio transmitter and ESCs completed, the team decided to record every step in the report and list it down in the Gitlab page as the same steps will be implemented on the calibration of the flight controller which will be used for the project. The team also decided to study more about the Mission Planner software and familiarize with its functionality.

 

 Next Step

 

The next step that has to be focused was to figure out the ways to control the servo and also the control of the sprayer system which is our main payload. The teammates are advised to brainstorm the idea of controlling the servos and the sprayer system.

 

 

 

**Log Report IDP Aerospace Design 2021**

Log 8 (Week 10)

 

Agenda

 

Friday (24/12/2021)

Unfortunately there was Covid 19 outbreak in laboratory H2.1. So the lab was shut down for one whole in order to be sanitized. So, no one from the Software and Control System team was able to go to laboratory and carry out the following task as per planned. Yet, the team decided to carry on tasks like writing report and updating the Gitlab. In addition, the compilation of firmware was still carried out one of the teammate, Sharul.

 

Team Decisions

 

Although the teammates was not able to gather in laboratory and carry out tasks like compiling the firmwares and design the circuits for servos, the team still decided to proceeds with the report work meanwhile waiting to get back to laboratory to work on the components.

 

 Next Step

 

The next step that team decided to take was to decide how to control the rotation of the servos where it should direct the motors various ways. On the other hand, the team also was planning on how to connect the sprayer system to the main control system and how to make a switch for the sprayer to turn it on or off.

 

 

 

 

**Log Report IDP Aerospace Design 2021**

Log 9 (Week 11)

 

Agenda

 

Friday  (31/12/2021)

As the Sprayer system team has managed to obtain the sprayer that they ordered, they requested the Software and Control System team to design the connection between sprayer and the main flight controller. One of Software and Control System team members, Nava took the job to design the connection between the flight controller and the sprayer. First of foremost, the sprayer pump was observed and analyzed, especially the wire connections from the sprayer pump. Before connecting to the flight controller, the Sprayer team wanted to test the sprayer pump could work with the power from a 6S LiPo battery. Hence, in order to test the sprayer, Nava, who was managing the wiring of the sprayer pump, soldered the live and ground wires to a female LiPo battery connector. Then, the pump was tested by connecting to a 6S LiPo battery, and the pump worked perfectly fine. Then the pump was connected to sprayer nozzle and the power supplied by the 6S LiPo battery was sufficient for the pump to create enough pressure on nozzle.



 Team Decisions

 

One of the Software and Control System members, Nava, have suggested the Sprayer team several method to control the sprayer pump after doing several researches. He advised  the sprayer pum can be controlled using relay switch or the flow regulator. Since the pump does not have a signal wire, it could not be connected directly to the flight controller. So, a relay switch can be used to turn on or off the sprayer pump or a flow regulator to regulate the flow rate of the pesticide from the pump. In other words, the relay switch or the flow regulate will be a medium of communication between the sprayer pump and the flight controller. Once the Sprayer team has been advised by Nava, they asked Dr Salah’s suggestion whether to use the relay switch or the flow regulator to be used. The main reason why the sprayer pump should be able to be turned on or off  is because sprayer pump used high power to produce the required pressure. So it is recommended the sprayer should turned off when it is not in use. Finally, the relay switch has been chosen to control the sprayer pump.

 

 

Next Step

 

As for the sprayer pump control, the Software and Control System team requested the Sprayer team to purchase the components needed to make the switch for the sprayer pump. It was panned to control the sprayer pump using smartphone instead of transmitter. The reason was the relay switch that has been suggested is Arduino powered. Hence, the Software and Control System team was not sure whether the relay switch can be connected to the flight controller or not. So, the relay switch was planned to connect to ESP8266 WiFi Module instead. Then the WiFi module will be controlled by smartphone to control the sprayer pump.







**Log Report IDP Aerospace Design 2021**

Log 9 (Week 12)

 

Agenda

 

Monday  (03/01/2022)

One of the Software and Control System team, Nava, joined the Sprayer team to purchase the components needed to make the relay switch in a robotic shop in Kajang, called RobotEdu.

The components that has been purchased were:

1. 12V Relay Switch

2. ESP8266 WiFi Module

3. Jumper Wires

4. ESP8266 Power Module

 

Once the module has been bought, they were brought to laboratory H2.1 in order to carry out the relay switch test. First and foremost, the relay switch was tested by connecting to the ESP8266. In order the ESP8266, the WiFi module was connected to the Ardupilot flight controller. THe ESP8266 WiFi module and the relay switch worked well and the connection between the WiFi module and the flight controller was correct. One of the main role player of Flight System Integration team, Zaim, advised that the relay switch can be directly connected to the flight controller and can be controlled by the radio transmitter as he had watch several tutorial video related to relay switches before. Hence, the Software and Control System team changed its plan to use ESP8266 WiFI module to connect the relay switch directly to the flight controller.

 

Tuesday  (04/01/2022)

Following Zaim’s  suggestion and idea, the control system proceeds with the progress where the relay switch was directly to the Ardupilot flight controller and tested. However, the parameters for the relay switch cannot be set in the Mission Planner because the Ardupilot flight controller board was outdated and retired. In addition, the FSi6AB receiver also was not working. The person behind that matter was unidentified. Hence, the receiver was replaced with Futaba  14CH PWM receiver. Since the Ardupilot flight controller can’t be used, one of Software and Control System member, Nava used the Pixhawk 4 Cube flight controller, which was used earlier for the calibration of ESCs. However, Fiqri pointed that the students are not allowed to use the Pixhawk Cube flight controller that has been used earlier because it is a propertyof laboratory H2.1 although it was used earlier during the calibration of ESCs. Hence, the Pixhawk 4 Cube flight controller was returned. On the other hand, other members of Software and Control System team discovered that the servos, which were connected to the Pixhawk Cube flight controller, which will be used for the Airship project, were not working because 5V of voltage supply was not supplied to the servos by the power distribution board of flight controller. In order to overcome that, BEC connectors were bought via online to supply the servos 5V of power supply from the power distribution board.

 

 

Team Decisions

 

The team decided on focusing everyone effort in the alteration of firmware and also the configuration of the servos until the BEC connectors. Once the team gets the BEC connectors, the servos will be tested and calibrated.

 

 

Next Step

 

Since the Ardupilot flight controller can’t be used for the configurations of the relay switch, the team decided to work on the Pixhawk flight controller that will be used for the Airship project. For that the team was waiting for clearance to work in the Pixhawk flight controller.  

 

 

 

**Log Report IDP Aerospace Design 2021**

Log 10 (Week 13)

 

Agenda

 

 

Wednesday  (19/01/2022)

Since the Ardupilot flight controller can’t be used to program the relay switch, the Software and Control System team decided to work directly on the Pixhawk Cube flight controller, which will be used for the Airship project. In order to make sure the team gets clearance to use the flight controller, all members of Flight System Integration team and sprayer team were invited to laboratory by 12.30pm. One of the Software and Control System team members, Nava, bought 5V relay switch and arrived the laboratory at 12.30pm. However, none from the Flight System Integration team as well as Software and Control System team except Nava were there. After waiting until 1.30pm, the programming process of the relay switch had begun although there were no one from Flight System Integration team members was there. The programming process includes:

1. Assigning a Auxiliary in a new Channel in the transmitter

2. Connecting the signal wire of the relay switch to a Auxiliary output of the flight controller.

3. Assigning the output Auxiliary to the cahnel created earlier.

PS: No pre-done settings and configurations were changed or disturbed in the process. 

 

Then, two members from Sprayer team, Yamunan and Theeban, and a member from Software and Control System team, Aliff,  joined Nava in the configuration of relay switch process. Then, Fiqri came and pointed out that the three people, Nava, Yamunan and Theeban, who were in the configuration process are not allowed to used the flight controller as they wish although the Flight System Integration team was informed earlier about the plan about to be carried out that day. Hence, the whole configuration process was stopped and decided to wait for more clearance.

 

Friday  (21/01/2022)

For the first time, a face-toface progress meeting was held in laboratory H2.1. Dr Salah was present to carry out the meeting. As for the Software and Control System team, Sharul represented the team and presented the progress which has been done so far. He pointed out the Software and Control System team was currently waiting for the BEC connectors to arrived. Meanwhil, the team is currently is working on the firmware to control servos and the configurations of the servo. At the end of the presentation, the Software and Control System team was informed to drop the idea of having a relay switch as the sprayer will be not attached to the Airship as per planned earlier. Dr Salah reminded that the Airship should be able to fly on Friday of Week 14.

 

Team Decisions

 

The team decided on focusing everyone effort in the alteration of firmware and also the configuration of the servos until the BEC connectors. Once the team gets the BEC connectors, the servos will be tested and calibrated.

 

Comment

 

The incident that happened on 05/01/2022 made a huge impact in the Software and Control System team. The initial plan on that was to carry out the whole configuration process of the relay switch with the presence of Flight System Integration team. Since, none of the team members arrived in laboratory on time, Nava (team member of Software and Control System team) had to start configuration process by his own as he had his FYP meeting at 3pm. The Software and Control System team was informed to not use the property of laboratory without permission by Fiqri. He also advised the team to use the components of the Airship project in order to do the configuration. On that day of incident, Nava was using the flight controller which is being used for the Airship project. The transmitter that has been used by him was the transmitter that will be used for the project. Another important point that has been noted was there was no alteration or changes were made on the configuration that has been done on the flight controller. Yet Nava and the other two members of Sprayer team, Yamunan and Theeban, were shouted by Fiqri in front of pupils. However, the people involved understand that the Airship project is as high stake project and decided to ignore the incident.







**Log Report IDP Aerospace Design 2021**

Log 11 (Week 14)

 

Agenda



Wednesday  (26/01/2022)

The Sprayer team requested the Software and Control System to work on the relay switch once again. One of the team members, Nava was assigned to perform that task. As for the switch of the sprayer pump, it was designed to remotely controlled from smartphone as per suggested earlier. Najme, a team member of Software and Control, volunteered to purchase the additional components needed for the relay switch, which was Lithium Ion batteries. The sprayer pump consumes a significant amount of power. If the sprayer pump connected to our main power source, which is lithium polymer battery, the power supply for the main control system and the motors of the Airship won’t be sufficient. Hence, a separate power source was decided to be used for the sprayer pump. The 6s Lithium Ion battery was build by soldering 6 lithium ion batteries together in series connection. This task was performed by Nava.

 

Thursday  (27/01/2022)

Once the 6s Lithium ion battery was built, the next step was to program and connect the relay switch to the sprayer pump. The ESP8266 was coded using Arduino IDE and connected to Smartphone to remotely control. Once the configuration was over, the 6S Lithium Ion battery and the sprayer pump was connected to the relay switch. Finally, the relay switch was to be controlled by smartphone. A series of test testing was done in order to make sure the relay switch works well. On the other hand, the final calibration of servos and ESC was carried out by the Software and Control System team.

 

 

Friday  (21/01/2022)

On the day of flight test of the Airship, the sprayer pump witht he relay switch was demonstrated to Dr Salah. During the flight test of Airship, the first problem faced was one of the servos did not work. After checking the connections for hours, the problem in the connection could not be identified. The second problem faced was one of the motors went out of control and the Airship went down as one of the thruster arm broke. Dr Salah called it a day, and everyone went back to laboratory for postmortem meeting. Dr Salah appreciated everyone’s effort throughout the project. The problem faced during the flight test was discussed. Dr Salah advised to add the problem faced during the flight test in our reports for the reference of upcoming batch of pupils.  

 

Comment

 

The problem where one of the servo didn’t work might be because the connection between the servo and the power distribution was faulty. Other than that, the servo might have some mechanical problem which might cause it to not rotate. The second problem faced during the flight test which was one of the motor went out of control might be because the configuration of motor might not done well. The configuration number of motor might not declared properly in the Mission Planner software which cause the flight controller to send recovering action signal to the wrong motor. Despite the problem all the forth year students of Aerospace Engineering managed to finish the project and made the Airship fly. A lot of experience and knowledge was gained from this project. 



